# OpenML dataset: Australian-Electricity-Demand

https://www.openml.org/d/46237

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Australian Electricity Demand forecasting data, half-hourly data.

From original source:
-----
This dataset contains 5 time series representing the half hourly electricity demand of 5 states in Australia: Victoria, New South Wales, Queensland, Tasmania and South Australia. It was extracted from R tsibbledata package.

-----

They claim that the original data comes from the R tsibbledata package, but I could not find the original data in the package or the original source.

There are 5 columns:

id_series: The id of the time series.

date: The date of the time series in the format "%Y-%m-%d %H:%M:$S".

time_step: The time step on the time series.

covariate_0: Covariate values of the time series, tied to the 'id_series'. Not interested in forecasting, but can help with the forecasting task.

value_0: The values of the time series, which will be used for the forecasting task.

Preprocessing:

1 - Renamed columns 'series_name', 'series_value', 'state' to 'id_series', 'value_0', and 'covariate_0'.

2 - Exploded the 'value_0' column.

3 - Created 'time_step' column from the exploded data.

4 - Created 'date' column from 'starting_date' and 'time_step'.

5 - Casted 'date' to str, 'time_step' to int, 'value_0' to float, and defined 'id_series' and 'covariate_0' as 'category'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46237) of an [OpenML dataset](https://www.openml.org/d/46237). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46237/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46237/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46237/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

